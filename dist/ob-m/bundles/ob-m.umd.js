(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('ob-m', ['exports', '@angular/common', '@angular/core'], factory) :
    (factory((global['ob-m'] = {}),global.ng.common,global.ng.core));
}(this, (function (exports,common,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ObMService = /** @class */ (function () {
        function ObMService() {
        }
        ObMService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        ObMService.ctorParameters = function () { return []; };
        /** @nocollapse */ ObMService.ngInjectableDef = i0.defineInjectable({ factory: function ObMService_Factory() { return new ObMService(); }, token: ObMService, providedIn: "root" });
        return ObMService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ObMComponent = /** @class */ (function () {
        function ObMComponent() {
        }
        /**
         * @return {?}
         */
        ObMComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        ObMComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-ObM',
                        template: "\n    <p>\n      ob-m works! -- powered by Milad :)\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        ObMComponent.ctorParameters = function () { return []; };
        return ObMComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SampleComponent = /** @class */ (function () {
        function SampleComponent() {
        }
        /**
         * @return {?}
         */
        SampleComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        SampleComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-sample',
                        template: "<p>\n  sample works! salam salam\n</p>\n",
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        SampleComponent.ctorParameters = function () { return []; };
        return SampleComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SampleModule = /** @class */ (function () {
        function SampleModule() {
        }
        SampleModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [SampleComponent],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [SampleComponent]
                    },] }
        ];
        return SampleModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ObMModule = /** @class */ (function () {
        function ObMModule() {
        }
        ObMModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [ObMComponent],
                        imports: [
                            SampleModule
                        ],
                        exports: [SampleModule, ObMComponent]
                    },] }
        ];
        return ObMModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.ObMService = ObMService;
    exports.ObMComponent = ObMComponent;
    exports.ObMModule = ObMModule;
    exports.SampleModule = SampleModule;
    exports.ɵa = SampleComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=ob-m.umd.js.map