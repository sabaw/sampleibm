import { CommonModule } from '@angular/common';
import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ObMService {
    constructor() { }
}
ObMService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ObMService.ctorParameters = () => [];
/** @nocollapse */ ObMService.ngInjectableDef = defineInjectable({ factory: function ObMService_Factory() { return new ObMService(); }, token: ObMService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ObMComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ObMComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-ObM',
                template: `
    <p>
      ob-m works! -- powered by Milad :)
    </p>
  `
            }] }
];
/** @nocollapse */
ObMComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SampleComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SampleComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-sample',
                template: "<p>\n  sample works! salam salam\n</p>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
SampleComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SampleModule {
}
SampleModule.decorators = [
    { type: NgModule, args: [{
                declarations: [SampleComponent],
                imports: [
                    CommonModule
                ],
                exports: [SampleComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ObMModule {
}
ObMModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ObMComponent],
                imports: [
                    SampleModule
                ],
                exports: [SampleModule, ObMComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ObMService, ObMComponent, ObMModule, SampleModule, SampleComponent as ɵa };

//# sourceMappingURL=ob-m.js.map