/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var ObMComponent = /** @class */ (function () {
    function ObMComponent() {
    }
    /**
     * @return {?}
     */
    ObMComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ObMComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-ObM',
                    template: "\n    <p>\n      ob-m works! -- powered by Milad :)\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    ObMComponent.ctorParameters = function () { return []; };
    return ObMComponent;
}());
export { ObMComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ItbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9vYi1tLyIsInNvdXJjZXMiOlsibGliL29iLW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBRWxEO0lBV0U7SUFBZ0IsQ0FBQzs7OztJQUVqQiwrQkFBUTs7O0lBQVI7SUFDQSxDQUFDOztnQkFkRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFFBQVEsRUFBRSxtRUFJVDtpQkFFRjs7OztJQVFELG1CQUFDO0NBQUEsQUFoQkQsSUFnQkM7U0FQWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLU9iTScsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBvYi1tIHdvcmtzISAtLSBwb3dlcmVkIGJ5IE1pbGFkIDopXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIE9iTUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iXX0=