import { CommonModule } from '@angular/common';
import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ObMService = /** @class */ (function () {
    function ObMService() {
    }
    ObMService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    ObMService.ctorParameters = function () { return []; };
    /** @nocollapse */ ObMService.ngInjectableDef = defineInjectable({ factory: function ObMService_Factory() { return new ObMService(); }, token: ObMService, providedIn: "root" });
    return ObMService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ObMComponent = /** @class */ (function () {
    function ObMComponent() {
    }
    /**
     * @return {?}
     */
    ObMComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ObMComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-ObM',
                    template: "\n    <p>\n      ob-m works! -- powered by Milad :)\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    ObMComponent.ctorParameters = function () { return []; };
    return ObMComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var SampleComponent = /** @class */ (function () {
    function SampleComponent() {
    }
    /**
     * @return {?}
     */
    SampleComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    SampleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-sample',
                    template: "<p>\n  sample works! salam salam\n</p>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    SampleComponent.ctorParameters = function () { return []; };
    return SampleComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var SampleModule = /** @class */ (function () {
    function SampleModule() {
    }
    SampleModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [SampleComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [SampleComponent]
                },] }
    ];
    return SampleModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ObMModule = /** @class */ (function () {
    function ObMModule() {
    }
    ObMModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ObMComponent],
                    imports: [
                        SampleModule
                    ],
                    exports: [SampleModule, ObMComponent]
                },] }
    ];
    return ObMModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ObMService, ObMComponent, ObMModule, SampleModule, SampleComponent as ɵa };

//# sourceMappingURL=ob-m.js.map