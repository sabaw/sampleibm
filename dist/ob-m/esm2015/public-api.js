/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of ob-m
 */
export { ObMService } from './lib/ob-m.service';
export { ObMComponent } from './lib/ob-m.component';
export { ObMModule } from './lib/ob-m.module';
export { SampleModule } from './lib/sample/sample.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL29iLW0vIiwic291cmNlcyI6WyJwdWJsaWMtYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSwyQkFBYyxvQkFBb0IsQ0FBQztBQUNuQyw2QkFBYyxzQkFBc0IsQ0FBQztBQUNyQywwQkFBYyxtQkFBbUIsQ0FBQztBQUNsQyw2QkFBYyw0QkFBNEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2Ygb2ItbVxuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL29iLW0uc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9vYi1tLmNvbXBvbmVudCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9vYi1tLm1vZHVsZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9zYW1wbGUvc2FtcGxlLm1vZHVsZSc7XG4iXX0=