/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { ObMComponent } from './ob-m.component';
import { SampleModule } from './sample/sample.module';
export class ObMModule {
}
ObMModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ObMComponent],
                imports: [
                    SampleModule
                ],
                exports: [SampleModule, ObMComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ItbS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9vYi1tLyIsInNvdXJjZXMiOlsibGliL29iLW0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNoRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFTdEQsTUFBTSxPQUFPLFNBQVM7OztZQVByQixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsWUFBWSxDQUFDO2dCQUM1QixPQUFPLEVBQUU7b0JBQ1AsWUFBWTtpQkFDYjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDO2FBQ3RDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9iTUNvbXBvbmVudCB9IGZyb20gJy4vb2ItbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgU2FtcGxlTW9kdWxlIH0gZnJvbSAnLi9zYW1wbGUvc2FtcGxlLm1vZHVsZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW09iTUNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBTYW1wbGVNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW1NhbXBsZU1vZHVsZSwgT2JNQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBPYk1Nb2R1bGUgeyB9XG4iXX0=