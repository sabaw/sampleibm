/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class ObMComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ObMComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-ObM',
                template: `
    <p>
      ob-m works! -- powered by Milad :)
    </p>
  `
            }] }
];
/** @nocollapse */
ObMComponent.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ItbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9vYi1tLyIsInNvdXJjZXMiOlsibGliL29iLW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBV2xELE1BQU0sT0FBTyxZQUFZO0lBRXZCLGdCQUFnQixDQUFDOzs7O0lBRWpCLFFBQVE7SUFDUixDQUFDOzs7WUFkRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFNBQVM7Z0JBQ25CLFFBQVEsRUFBRTs7OztHQUlUO2FBRUYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItT2JNJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8cD5cbiAgICAgIG9iLW0gd29ya3MhIC0tIHBvd2VyZWQgYnkgTWlsYWQgOilcbiAgICA8L3A+XG4gIGAsXG4gIHN0eWxlczogW11cbn0pXG5leHBvcnQgY2xhc3MgT2JNQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiJdfQ==