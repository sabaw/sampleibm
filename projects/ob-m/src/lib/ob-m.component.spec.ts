import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObMComponent } from './ob-m.component';

describe('ObMComponent', () => {
  let component: ObMComponent;
  let fixture: ComponentFixture<ObMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
