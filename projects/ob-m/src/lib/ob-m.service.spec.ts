import { TestBed } from '@angular/core/testing';

import { ObMService } from './ob-m.service';

describe('ObMService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObMService = TestBed.get(ObMService);
    expect(service).toBeTruthy();
  });
});
