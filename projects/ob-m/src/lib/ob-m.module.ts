import { NgModule } from '@angular/core';
import { ObMComponent } from './ob-m.component';
import { SampleModule } from './sample/sample.module';

@NgModule({
  declarations: [ObMComponent],
  imports: [
    SampleModule
  ],
  exports: [SampleModule, ObMComponent]
})
export class ObMModule { }
