/*
 * Public API Surface of ob-m
 */

export * from './lib/ob-m.service';
export * from './lib/ob-m.component';
export * from './lib/ob-m.module';
